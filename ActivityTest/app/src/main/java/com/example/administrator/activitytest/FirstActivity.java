package com.example.administrator.activitytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class FirstActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String TAG  = "FirstActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_first);
        setTitle("FirstActivity--"+getTaskId());
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case  R.id.btn1:
                intent = new Intent(FirstActivity.this,FirstActivity.class);
                startActivity(intent);
                break;
            case R.id.btn2:
                intent = new Intent(FirstActivity.this,SecondActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//        Log.d(TAG, "onPostResume");
//    }
}
