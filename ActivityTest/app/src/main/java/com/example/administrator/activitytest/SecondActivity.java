package com.example.administrator.activitytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String TAG = "SecondActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        setTitle("SecondActivity--"+getTaskId());
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case R.id.btn3:
                intent = new Intent(SecondActivity.this,FirstActivity.class);
                startActivity(intent);
                break;
            case R.id.btn4:
                intent = new Intent(SecondActivity.this,ThirdActivity.class);
                startActivity(intent);
                break;
            case R.id.btn5:
                intent = new Intent(SecondActivity.this,SecondActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
