package com.example.administrator.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if(v.getId()==R.id.btnS){
            intent = new Intent(SecondActivity.this,FirstActivity.class);
            startActivity(intent);
        }
    }
}
