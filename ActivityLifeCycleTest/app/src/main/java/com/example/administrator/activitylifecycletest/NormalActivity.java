package com.example.administrator.activitylifecycletest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class NormalActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        if(v.getId() == R.id.btnM){
            intent = new Intent(NormalActivity.this,MainActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.btnD){
            intent = new Intent(NormalActivity.this,DialogActivity.class);
            startActivity(intent);
        }
    }
}
